<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    function CreateUnit(Request $request){
            DB::beginTransaction(); 
            try{

                $kavling = $request->input('kavling'); 
                $blok = $request->input('blok');
                $no_rumah = $request->input('no_rumah');
                $harga_rumah = $request->input('harga_rumah');
                $luas_tanah = $request->input('luas_tanah');
                $luas_bangunan = $request->input('luas_bangunan');


            
                $usr = new unit; 
                $usr->kavling = $kavling; 
                $usr->blok = $blok; 
                $usr->no_rumah = $no_rumah;
                $usr->harga_rumah = $harga_rumah;
                $usr->luas_tanah = $luas_tanah;
                $usr->luas_bangunan = $luas_bangunan;
                $usr->save();

                // $unit = UnitRumah::get();

                DB::commit();
                // return response()->json($usrList, 200);
            }

            catch(\Exception $e){
                DB::rollBack();
                return response()->json(["message" => $e->getMessage()],500);

            }
        }
        
        function DeleteUnit(Request $request){
            DB::delete('delete from user where id = ?', [$request->delete]);
            return redirect('/');
        }
}
